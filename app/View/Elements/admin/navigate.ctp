<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/admin/dashboards">Manager</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <?php echo $this->Html->link('<i class="fa fa-user fa-fw"></i> User Profile',array('admin'=>true,'controller'=>'users','action'=>'edit',$current_user['id']),array('escape'=>false));?>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="http://localhost:8080/cake2/doctruyen/admin/users/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Users<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/users/list">List</a>
                        </li>
                        <li>
                            <?php echo $this->Html->link('Add',array('controller'=>'users','action'=>'add'),array('escape'=>false))?>
                            <!--Or-->
                            <!--<a href="/admin/users/add">Add</a>-->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Category<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/categories/list">List</a>
                        </li>
                        <li>
                            <?php echo $this->Html->link('Add',array('controller'=>'categories','action'=>'add'),array('escape'=>false))?>
                            <!--Or-->
                            <!--<a href="/admin/users/add">Add</a>-->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-book"></i> Story<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/stories/list">List</a>
                        </li>
                        <li>
                            <?php echo $this->Html->link('Add',array('controller'=>'stories','action'=>'add'),array('escape'=>false))?>
                            <!--Or-->
                            <!--<a href="/admin/users/add">Add</a>-->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-file-image-o"></i> Chapter<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/chapters/list">List</a>
                        </li>
                        <li>
                            <?php echo $this->Html->link('Add',array('controller'=>'chapters','action'=>'add'),array('escape'=>false))?>
                            <!--Or-->
                            <!--<a href="/admin/users/add">Add</a>-->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-file-image-o"></i> Slide<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/slides/index">List</a>
                        </li>
                        <li>
                            <?php echo $this->Html->link('Add',array('controller'=>'slides','action'=>'add'),array('escape'=>false))?>
                            <!--Or-->
                            <!--<a href="/admin/users/add">Add</a>-->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>